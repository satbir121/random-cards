const SUITS = ["Heart", "Diamond", "Club", "Spade"];
const MAX_CARDS = 52;

export function populateCards() {
  const cards = [];
  for (let i = 0; i < MAX_CARDS; i++) {
    const number = i % 13;
    const suit = Math.floor(i / 13);
    cards.push({
      suit: SUITS[suit],
      number: number
    });
  }
  return cards;
}

export function getNRandomCards(drawnCardIndices, n) {
  let newCards = [];
  const cardsToWithdraw = Math.min(MAX_CARDS - drawnCardIndices.length, n);

  while (newCards.length < cardsToWithdraw) {
    const index = Math.floor(Math.random() * MAX_CARDS);
    if (!drawnCardIndices.includes(index) && !newCards.includes(index)) {
      newCards.push(index);
    }
  }
  return newCards;
}
