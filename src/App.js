import "./App.css";
import React, { useReducer } from "react";
import Card from "./Card";
import { populateCards, getNRandomCards } from "./utils";

const initialState = {
  cards: populateCards(),
  drawnCardIndices: []
};

function reducer(state, action) {
  switch (action.type) {
    case "draw":
      const newCards = getNRandomCards(state.drawnCardIndices, 5);
      const newDrawnCardIndices = state.drawnCardIndices.concat(newCards);
      return {
        ...state,
        drawnCardIndices: newDrawnCardIndices
      };
    default:
      throw new Error();
  }
}

export default function App() {
  const [state, dispatch] = useReducer(reducer, initialState);
  return (
    <div className="App">
      <button className="draw" onClick={() => dispatch({ type: "draw" })}>Draw</button>
      <div className="drawn-cards">
        {state.drawnCardIndices.map((cardIndex, index) => {
          const card = state.cards[cardIndex];
          return <Card key={index} {...card} />;
        })}
      </div>
    </div>
  );
}
