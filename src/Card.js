import React from "react";
import "./Card.css";

export default function Card(props) {
  console.log(props);
  return (
    <div className="card">
        <div className="number-above">{props.number}</div>
        <img className="card-suit" src={props.suit + ".png"}></img>
        <div className="number-below">{props.number}</div>
    </div>
  );
}

